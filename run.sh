#!/bin/bash

if [[ $(cat /proc/version | grep Microsoft) ]]
then
	bin/godot.windows.opt.tools.64.exe moon-tanks/project.godot
else
	pkill -f godot
	bin/godot.x11.opt.tools.64 moon-tanks/project.godot
fi

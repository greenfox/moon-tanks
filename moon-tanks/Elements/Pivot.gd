extends Node

var GameManager:GameManager
var CurrentClientPlayer:ShooterController
var Planet:PlanetOne
var CurrentLevel:Node
var CurrentEncounter:Spatial

var currentScore:int = 0

func reset():
	CurrentClientPlayer = null
	Planet = null
	CurrentLevel = null
	CurrentEncounter = null
	currentScore = 0
	$"/root/PlanetFinal".queue_free()
	$"/root".add_child(load("res://Levels/PlanetFinal.tscn").instance())


func getRandomBasis(up:Vector3)->Basis:
	var output:Vector3 = (Vector3(randf(),randf(),randf()) * 2) - Vector3.ONE
	
	up = up.normalized()
	var back:Vector3 = (output - output.project(up)).normalized()
	var right:Vector3 = up.cross(back)
	
	return Basis(right,up,back)



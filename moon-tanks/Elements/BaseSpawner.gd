extends Spatial

class_name BaseSpawner

export(float) var displaceHight = 0
export(PackedScene) var SceneToSpawn

export(bool) var active = false


func _ready():
	if not active:
		queue_free()
		return
	assert(SceneToSpawn,"Spawner Needs Scene!")
	call_deferred("spawnAfterSceneReady")
	
func spawnAfterSceneReady():
	var spawned:Spatial = SceneToSpawn.instance()
	get_parent().add_child(spawned)
	if(spawned.has_method("selfPositioning")):
		spawned.selfPositioning(global_transform,get_parent().VoxelPlanet)
	else:
		spawned.global_transform = get_parent().VoxelPlanet.getSurfaceTransform(global_transform)
		spawned.global_transform.origin += spawned.global_transform.origin.normalized() * displaceHight
	queue_free()

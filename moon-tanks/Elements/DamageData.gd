extends Reference
class_name DamageData

enum DAMAGE_TYPE {
	EXPLOTION, LASER
}

export(float) var value = 0
export(DAMAGE_TYPE) var type = DAMAGE_TYPE.EXPLOTION

var source setget setSource

var team = null


func make(newOwner):
	source = newOwner
	return self

func canDamage(target):
	return target.has_method("takeDamage")

func checkTeam(target:Node):
	return target.has_method("onTeam") and target.onTeam(team)

	
func damage(target):
	if target is VoxelLodTerrain:
		return true
	if target.has_method("takeDamage"):
		target.takeDamage(self)
		return true
	else:
		return false


func setSource(value):
	source = value
	team = source.team
	

extends Camera

export var flySpeed = 100
export var sprintMult = 10

export var controlsPath:NodePath 

var fpsRotation:Vector2 = Vector2.ZERO;
var rotationSpeed:float = 0.001;

func _process(delta):
	var Controls = get_node(controlsPath)
	if Controls == null:
		assert(false, "ERROR FLY GAME NEEDS A CONTROLS PATH!")
		return
	transform.basis = Basis()
	transform.basis = transform.basis.rotated(Vector3.RIGHT,fpsRotation.y)
	transform.basis = transform.basis.rotated(Vector3.UP,fpsRotation.x)
#	print({fpsRotation=fpsRotation,basis=transform.basis})

	var a :ControlsContainer= Controls.get_controls()
	a.movement += (Vector3.UP* int(a.jumped)) + (Vector3.DOWN*int(a.crouch))
	a.renormalize()

	var deltaMovement = transform.basis.xform(a.normalized * flySpeed * delta)
	if Input.is_action_pressed("Sprint"):
		translation += deltaMovement * sprintMult
	else:
		translation += deltaMovement
		


func _unhandled_input(event):
	if event is InputEventMouseMotion:
		if event.button_mask == 2:
			fpsRotation -= event.relative * rotationSpeed
#			fpsRotation.x = fpsRotation.x * rotationSpeed
			
	if event is InputEventMouseButton:
		if event.button_mask == 2:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
#	if event is InputEventMouse and :
#		if event is InputEventMouseMotion:
#			pass
	

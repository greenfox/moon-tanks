extends BasePlayer

class_name ShooterController

export var grav : float = 25.0 #maybe this needs to be informed by the world?
export var jumpImpulse : float = 10.0 #maybe this needs to be informed by the world?
export var moveSpeed : float = 100.0
export var maxSpeed : float = 100.0
export var slowRate:float = 0.05

export(float) var maxHealth = 100
var health = maxHealth
#export var rotationAccel:float = 0.005
#export var rotationMax:float = 0.25
#export var rotationDecel:float = 0.05

export var maxTurnRate:float = 1.0
var turnTarget:float
var pitch:float = 0

var currentVelocity:Vector3 = Vector3.ZERO
var rotationVelocity:float = 0.0

var up:Vector3

enum GRAVITYTYPE { ORIGIN, YAXIS }

export(GRAVITYTYPE) var gravType = GRAVITYTYPE.ORIGIN

export(NodePath) var ControlsNode
	
signal fire(delta,target)
	
func _ready():
	Pivot.CurrentClientPlayer = self
	var ctrlNode = get_node(ControlsNode)
	assert(ctrlNode and ctrlNode.has_method("get_controls"), "ERROR MISSING CONTROL NODE WITH `get_controls` METHOD")

func _physics_process(delta):
	updateVars()
	var controls :ControlsContainer= get_node(ControlsNode).get_controls()
	currentVelocity = processControls(delta,controls)
	currentVelocity = move_and_slide(currentVelocity,up,true,1,deg2rad(80),true)
	
	if is_on_floor():
		if (currentVelocity.length() - (grav*delta)) < 0.025: #and controls.movement == Vector3.ZERO:
			currentVelocity = Vector3.ZERO
		else:
			currentVelocity = lerp(currentVelocity,Vector3.ZERO,slowRate*delta)
		
	if health < 0:
		var death :Spatial= preload("res://DeathSequence.tscn").instance()
		get_parent().add_child(death)
		death.global_transform = global_transform
		queue_free()


func updateVars():
#	up = Vector3.UP
	if translation != Vector3.ZERO:
		up = translation.normalized()
	else:
		up = Vector3.UP
		
func processControls(delta:float,controls:ControlsContainer)->Vector3:
	if controls.isFiring:
		emit_signal("fire",delta,getTarget())
	debug.mouse = controls.look
	var gravImpulseVector = -up * grav * delta
	
	var controlsImpulse : Vector3 = Vector3.ZERO

	#if on floor, push player
	if is_on_floor():
		controlsImpulse = controls.normalized * delta * moveSpeed
		if controls.jumped:
			controlsImpulse = Vector3.UP * jumpImpulse 
	else:
		pass #todo: add airtime

	#adjust transform to make world "up" our current "up"
	var quat:Quat = transform.basis.get_rotation_quat()
	var f = quat.xform(Vector3.FORWARD)
	f = (f - f.project(up) ).normalized() #corrected by new up
	var left = f.cross(up)
	transform.basis = Basis(left,up,-f)

	controlsImpulse = transform.basis.xform(controlsImpulse)
	
	#turning from controls.look
	turnTarget += controls.look.x
	if turnTarget > PI:
		turnTarget-= TAU
	elif turnTarget < -PI:
		turnTarget+= TAU
	var turnFrame = (maxTurnRate*delta)
	if turnTarget > turnFrame:
		transform = transform.rotated(up,turnFrame)
		turnTarget -= turnFrame
	elif turnTarget < -turnFrame:
		transform = transform.rotated(up,-turnFrame)
		turnTarget += turnFrame
	else:
		transform = transform.rotated(up,turnTarget)
		turnTarget = 0
#	turnTarget *= 1 - (1 *delta)
	debug.turnTarget = turnTarget
	$CameraGimble.transform.basis = Basis(Vector3.UP,turnTarget)

	#pitch
	pitch = clamp(pitch + controls.look.y, -PI*0.5, PI*0.5)
	debug.pitch = pitch
	$CameraGimble.transform.basis = $CameraGimble.transform.basis.rotated($CameraGimble.transform.basis.xform(Vector3.RIGHT),pitch) 

	$AimingGimble.transform.basis = Basis(Vector3.UP,0).rotated(Vector3.RIGHT,pitch)

	
	var newVelocity :Vector3= currentVelocity + gravImpulseVector + controlsImpulse
	if newVelocity.length() > maxSpeed:
		newVelocity = newVelocity.normalized() * maxSpeed
	return newVelocity


func getCameraTransform()->Transform:
	return $CameraMount.global_transform

var debug = {}

func getDebugData():
	return debug

func getTarget()->Vector3:
	return $AimingGimble.getTarget()

func takeDamage(damage:DamageData):
	health -= damage.value

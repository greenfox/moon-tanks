extends KinematicBody

class_name TankController

export var grav : float = 25.0 #maybe this needs to be informed by the world?
export var jumpImpulse : float = 10.0 #maybe this needs to be informed by the world?
export var moveSpeed : float = 100.0
export var maxSpeed : float = 100.0
export var SlowRate:float = 0.05

export var rotationAccel:float = 0.005
export var rotationMax:float = 0.25
export var rotationDecel:float = 0.05


var currentVelocity:Vector3 = Vector3.ZERO
var rotationVelocity:float = 0.0

var up:Vector3

enum GRAVITYTYPE { ORIGIN, YAXIS }

export(GRAVITYTYPE) var gravType = GRAVITYTYPE.ORIGIN

export(NodePath) var ControlsNode
	
func _ready():
	var ctrlNode = get_node(ControlsNode)
	assert(ctrlNode and ctrlNode.has_method("get_controls"), "ERROR MISSING CONTROL NODE WITH `get_controls` METHOD")

func _physics_process(delta):
	updateVars()
	
	currentVelocity = processControls(delta,get_node(ControlsNode).get_controls())
	currentVelocity = move_and_slide(currentVelocity,up)
	
	if is_on_floor():
		currentVelocity = lerp(currentVelocity,Vector3.ZERO,SlowRate)
		


func updateVars():
#	up = Vector3.UP
	if translation != Vector3.ZERO:
		up = translation.normalized()
	else:
		up = Vector3.UP
		
func processControls(delta:float,controls:ControlsContainer)->Vector3:
	debug.mouse = controls.look
	var gravImpulseVector = -up * grav * delta
	
	var controlsImpulse : Vector3 = Vector3.ZERO
	if is_on_floor():
		controlsImpulse = Vector3(0,0,controls.normalized.z * delta * moveSpeed)
		
		if controls.normalized.x:
			rotationVelocity = lerp(rotationVelocity,-controls.normalized.x*rotationMax,rotationAccel)
		else:
			rotationVelocity = lerp(rotationVelocity,0,rotationDecel)

		#if controls.jumped:
		#	controlsImpulse = Vector3.UP * jumpImpulse 
	else:
		pass #todo: add airtime


	var quat:Quat = transform.basis.get_rotation_quat()
	var f = quat.xform(Vector3.FORWARD)
	f = (f - f.project(up) ).normalized() #corrected by new up
	var left = f.cross(up)
	transform.basis = Basis(left,up,-f)

	controlsImpulse = transform.basis.xform(controlsImpulse)
	
	transform = transform.rotated(up,rotationVelocity)
	var newVelocity :Vector3= currentVelocity + gravImpulseVector + controlsImpulse
	if newVelocity.length() > maxSpeed:
		newVelocity = newVelocity.normalized() * maxSpeed
	return newVelocity


func getCameraTransform()->Transform:
	return $CameraMount.global_transform

var debug = {}

func getDebugData():
	return debug

extends KinematicBody

class_name BasePlayer

export(int) var team = 1

func isPlayer()->bool:
	return true


func onTeam(checkTeam)->bool:
	return checkTeam == team

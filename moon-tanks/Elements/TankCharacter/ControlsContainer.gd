class_name ControlsContainer


var movement : Vector3 = Vector3.ZERO
var normalized : Vector3 = Vector3.ZERO
var jumped:bool = false
var crouch:bool = false

var look : Vector2 = Vector2.ZERO

var isFiring:bool = false

func renormalize()->ControlsContainer:
	normalized = movement.normalized()
	return self

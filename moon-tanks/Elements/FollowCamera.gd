extends Camera

export var targetPath:NodePath
export var rate = 0.1


func _ready():
	var target = get_node(targetPath)
	if target and target.has_method("getCameraTransform"):
		global_transform = target.getCameraTransform()
	else:
		print("Error, follow camera needs a targetPath and that object needs a getCameraTransform")

func _process(delta):
	var target = get_node(targetPath)
	if target and target.has_method("getCameraTransform"):
		var targetTransform = target.getCameraTransform()
		global_transform = global_transform.interpolate_with(targetTransform,rate)
	else:
		print("Error, follow camera needs a targetPath and that object needs a getCameraTransform")

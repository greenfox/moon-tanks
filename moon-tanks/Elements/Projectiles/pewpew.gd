extends AudioStreamPlayer3D

export(float) var attenuation = 0.0
export(float) var normal = 1.0

func _ready():
	call_deferred("reparent")
	pitch_scale = normal + ((randf()-0.5) * attenuation)

func reparent():
	var t = get_parent().damage.source
	get_parent().remove_child(self)
	t.add_child(self)

#	var a = get_node("../../")
#	var t = global_transform
#	get_parent().remove_child(self)
#	a.add_child(self)
#	global_transform = t

func _on_pewpew_finished():
	queue_free()

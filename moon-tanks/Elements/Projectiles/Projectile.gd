extends Area

class_name Projectile

export(float) var speed = 100;
export(float) var fradius = 3;
#var projectileOwner :Spatial= null

var startingVelocity:Vector3 = Vector3.ZERO

var damage:DamageData


func _ready():
	assert(damage,"Damage Data not set")
#	assert(projectileOwner,"This projectile is unowned!")
#	startingVelocity = damage.source.currentVelocity #+ (global_transform.basis * Vector3.FORWARD * speed)
	look_at(damage.source.global_transform.origin+damage.source.currentVelocity,global_transform.origin.normalized())
	pass # Replace with function body.
#	$AudioStreamPlayer3D.play()

func _physics_process(delta):
	global_transform.origin += (startingVelocity + global_transform.basis * (Vector3.FORWARD * speed)) * delta



func _on_Laser_body_entered(body):
	if not damage.checkTeam(body) and damage.damage(body):
		queue_free()


func _on_Laser_area_entered(area):
	_on_Laser_body_entered(area)

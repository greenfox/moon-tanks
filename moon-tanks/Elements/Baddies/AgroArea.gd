extends Area

func _ready():
	$CollisionShape.shape.radius = get_parent().agroRange

func _on_AgroArea_body_entered(body):
	# print("entered!")
	if body.has_method("onTeam") and not body.onTeam(get_parent().team):
		get_parent().targets[body] = body

func _on_AgroArea_body_exited(body):
	get_parent().targets.erase(body)

func getTeam():
	return get_parent().team


extends KinematicBody

class_name FlierBase

export(float) var hoverHeight
export(float) var agroRange
export(float) var targetEngagmentRange
export(int) var team = 2

export(float) var patrolRange = 250

export(float) var flySpeed = 10
export(float) var acceleration = 10
export(float) var maxSpeed = 10

export(float) var turnAccel = .2
export(float) var turnMaxSpeed = 0.05
var currentTurnRate = 0



export(float) var grav = 10



export(float) var health = 20



var up:Vector3

var currentVelocity:Vector3 = Vector3.ZERO
var flyTarget:Vector3
var targets = {}
var strafeDirection:Vector3


var home:Vector3

func _ready():
	$GroundSensor.cast_to = Vector3(0,-hoverHeight,0)


func _physics_process(delta):
	updateVars()
	var control:ControlsContainer = $AI.getControls(self)
	
	if control.isFiring:
		$GunMount.fire(getFirstTarget())
	
	currentVelocity += global_transform.basis.xform(control.normalized * delta * acceleration)
	if currentVelocity.length() > maxSpeed:
		currentVelocity = currentVelocity.normalized() * maxSpeed
	
#	if control
	if control.jumped:
		currentVelocity += up * grav * delta
	else:
		currentVelocity += up * -grav * delta

	var right :Vector3= global_transform.basis.xform(Vector3.RIGHT)
	right = (right - right.project(up)).normalized()
	var back = up.cross(right)
	global_transform.basis = Basis(right,up,back)
	
	#look processing
	if control.look.x:
		currentTurnRate = clamp(currentTurnRate + control.look.x * turnAccel * delta,-turnMaxSpeed,turnMaxSpeed)
	else:
		currentTurnRate = lerp(currentTurnRate,0,0.1)
	currentTurnRate = clamp(currentTurnRate,-turnMaxSpeed,turnMaxSpeed)
	global_transform.basis = global_transform.basis.rotated(up,currentTurnRate)

	var collision :KinematicCollision= move_and_collide(currentVelocity*delta)
	
	
#	$V.cast_to = global_transform.basis.xform_inv(currentVelocity * 10)
	
	if collision:
		health -= max(currentVelocity.length()-5,0);
		if health < 0:
			explode()
		else:
			currentVelocity = currentVelocity.bounce(collision.normal)
	

func explode():
	var explotion = preload("res://Elements/FX/Explotion.tscn").instance()
	explotion.radius = 2
	explotion.transform = global_transform
	get_parent().add_child(explotion)
	queue_free()
	


func selfPositioning(newTransform:Transform,VoxelPlanet:PlanetOne):
	global_transform = VoxelPlanet.getSurfaceTransform(newTransform)
	global_transform.origin += global_transform.basis.xform(Vector3(0,hoverHeight,0))


func _on_AgroArea_body_entered(body):
	if body.has_method("onTeam") and not body.onTeam(team):
		targets[body] = body


func _on_AgroArea_body_exited(body):
	targets.erase(body)


func onTeam(checkTeam)->bool:
	return checkTeam == team

	
func updateVars():
	up = global_transform.origin.normalized()



func takeDamage(damage:DamageData):
	health -= damage.value


func getDebugData():
	return "buzzardSpeed:" + String(currentVelocity.length())

func getFirstTarget()->Spatial:
	return targets[targets.keys()[0]]

func makeTarger(target)->Targeter:
	if target is Spatial:
		return Targeter.new(global_transform.xform_inv(target.global_transform.origin))
	elif target is Vector3:
		return Targeter.new(target)
	else:
		assert(false,"Error Invalid Target")
		return Targeter.new(Vector3.ZERO)

class Targeter:
	func _init(value):
		rebuildReletive(value)
	var reletive:Vector3 setget rebuildReletive
	var isLeft:bool
	var isRight:bool
	var isAbove:bool
	var isBelow:bool
	var distance:float
	func rebuildReletive(value):
		reletive = value
		distance = reletive.length()
		isRight = reletive.x > 0 
		isLeft = reletive.x < 0 
		isAbove = reletive.y > 0 
		isBelow = reletive.y < 0 
	func _to_string():
		return String(reletive)
		
		
		
		


func _on_Buzzard_tree_exiting():
	Pivot.currentScore += 5
	pass # Replace with function body.

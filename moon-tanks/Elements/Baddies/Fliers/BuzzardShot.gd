extends Area

class_name BuzzardShot

var damage:DamageData
export(float) var startingSpeed
export(float) var acceleration


var currentSpeed = startingSpeed
var target:Spatial

func _ready():
	assert(damage,"Damage data not set")

func _physics_process(delta):
	if Pivot.CurrentClientPlayer:
		currentSpeed += delta * acceleration
		$geomissile/AnimationPlayer.playback_speed = currentSpeed * 0.1
		var tn = global_transform.origin.normalized()
		var targetTransform = global_transform.looking_at(target.global_transform.origin+tn,global_transform.origin.normalized())
		global_transform.basis = global_transform.basis.get_rotation_quat().slerp(targetTransform.basis.get_rotation_quat(),delta*10)
		#	 = global_transform.basis.slerp(targetTransform.basis,delta*10)
		global_transform.origin += global_transform.basis * Vector3.FORWARD * delta * currentSpeed


func explode():
	if is_queued_for_deletion():
		return
	pass #spawn explotion
	var ex = preload("res://Elements/FX/Explotion.tscn").instance()
	ex.radius = 2
	ex.global_transform = global_transform
	get_parent().add_child(ex)
	queue_free()

func _on_BuzzardShot_body_entered(body:Spatial):
	if body == damage.source:
		return
	if not damage.checkTeam(body) and damage.damage(body):
		explode()


func takeDamage(damage:DamageData):
	explode()

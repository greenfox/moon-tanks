extends Spatial

export var localTime = 0.0
var mode: int
export(NodePath) var gunPath = ".."

const MODE_AIM = 0
const MODE_PRIME = 1
const MODE_FIRE = 2
const MODE_COOLDOWN = 3

const aimDuration = 2.25
const primeDuration = 0.75
const coolDown = 3.75
const aimLerpTime = 1.0 # time to line up aim after entering MODE_AIM

var home

var gun
var closestPlayerOrigin: Vector3
var cachedAngle: float

func _ready ( ):
	gun = get_node ( gunPath )
	mode = MODE_AIM
	localTime = randf ( )
	cachedAngle = rotation.y

func changeMode ( newMode: int ):
	cachedAngle = rotation.y
	mode = newMode
	#$gun.changeMode ( newMode )	
	localTime = 0

func _physics_process ( delta ) :
		
	localTime += delta
	
	if mode == MODE_AIM:
		aim ( delta )
	
	elif mode == MODE_PRIME:
		if localTime > primeDuration:
			changeMode ( MODE_FIRE )
	
	elif mode == MODE_COOLDOWN:
		rotation.y = cachedAngle + 0.5 * sin ( localTime )
		if localTime > coolDown:
			changeMode ( MODE_AIM )

func aim ( delta ):
	closestPlayerOrigin = getClosestPlayerOrigin ( )
	var playerLocal = get_global_transform ( ).xform_inv ( closestPlayerOrigin )
	var targetAngle = - atan2 ( playerLocal.z , playerLocal.x ) - 0.5 * PI
	
	if localTime < aimLerpTime:
		rotation.y = lerp ( cachedAngle , targetAngle , smooth ( localTime / aimLerpTime ) )
	else:
		rotation.y = targetAngle
	if localTime > aimDuration:
		changeMode ( MODE_PRIME)

func getClosestPlayerOrigin ( ) -> Vector3:
	
	var minDistance = INF
	var closestPlayerOrigin: Vector3
	for player in get_tree ( ).get_nodes_in_group ( "player" ):
		
		var playerOrigin = player.global_transform.origin
		var distance: = global_transform.origin.distance_squared_to \
			( playerOrigin )
		
		if distance < minDistance:
			closestPlayerOrigin = playerOrigin
			minDistance = distance
	
	return closestPlayerOrigin

func smooth ( f: float ) -> float:
	if f < 0.0: return 0.0
	if f > 1.0: return 1.0
	if f < 0.5: return 2.0 * f * f
	var g = 1.0 - f
	return 1.0 - 2.0 * g * g

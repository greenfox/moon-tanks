extends StaticBody

class_name Baddie_Structure

export(float) var health = 100
export(int) var team = 2

func _ready():
	pass

func _process(delta):
	if health < 0:
		var explotion = preload("res://Elements/FX/Explotion.tscn").instance()
		get_parent().add_child(explotion)
		explotion.global_transform = $ExplosionPosition.global_transform
		queue_free()
		

func takeDamage(damage:DamageData):
	health -= damage.value



func onTeam(value):
	return team == value

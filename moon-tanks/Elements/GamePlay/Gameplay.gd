extends Node

class_name GameManager

export(PackedScene) var pauseMenu = preload("res://Elements/UI/PauseMenu.tscn")
export(NodePath) var HUD
export(NodePath) var Planet

export(PackedScene) var MainPlayerSpawner
export(PackedScene) var EncounterSpawner


var currentPlayer

var ActiveEncounter:Spatial

func _ready():
	Pivot.GameManager = self
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	call_deferred("setupGame")

func setupGame():
	setupPOIs()
	var playerPosition = spawnPlayer()
	setupEncounter(playerPosition)

"""
scan for players.
while player exists
	scan for encounters
		if no encounters, spawn encoutner
		
		
encounter
	scan for buildings
	if no buildings, GC self
"""



func setupPOIs():
	$POI.setupPOIs()
	var p = $POI.points

func _process(delta):
	if Input.is_action_just_pressed("pauseMenu"):
		add_child(pauseMenu.instance())


func _pauseMenuClosed(menu):
	menu.queue_free()





func spawnPlayer():
	if Pivot.CurrentClientPlayer == null:
		var player:Spatial = MainPlayerSpawner.instance()
		var playerPosition = $POI.randPoint()
		player.global_transform.origin = playerPosition
		get_parent().add_child(player)
		return playerPosition

func nextEncounter():
	setupEncounter(Pivot.CurrentClientPlayer.global_transform.origin)

func setupEncounter(playerPosition):
	print('setting up encounter')
	if Pivot.CurrentEncounter != null:
		Pivot.CurrentEncounter.queue_free()
		
	var encounter:Spatial = EncounterSpawner.instance()
	encounter.global_transform.origin = $POI.randPointInRange(playerPosition,0.10,0.15)
	get_parent().add_child(encounter)
	ActiveEncounter = encounter
	encounter.connect("tree_exiting",self,"nextEncounter")


func resetGame():
	if Pivot.CurrentClientPlayer: Pivot.CurrentClientPlayer.queue_free()
	if Pivot.CurrentEncounter: Pivot.CurrentEncounter.queue_free()

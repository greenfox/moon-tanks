extends MarginContainer


func _ready():
	pass # Replace with function body.



func _process(delta):
	if Input.is_action_just_pressed("debug"):
		visible = !visible
	if visible:
		var debugData :Array = get_tree().call_group("debug","get_debug_data")
		for i in debugData:
			print(i)
			pass

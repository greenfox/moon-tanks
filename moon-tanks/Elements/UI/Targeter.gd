extends Node2D

export(float) var showThreshold = 175


func _process(delta):
	var player:Spatial = Pivot.CurrentClientPlayer
	var target:Spatial = Pivot.CurrentEncounter
	if not (player and target):
		visible = false
		return
	var reletivePoint:Vector3 = player.get_node("CameraGimble/Spatial/Camera").global_transform.xform_inv(target.global_transform.origin)

	if reletivePoint.length() > showThreshold:
		visible = true
		var angle = Vector2.UP.angle_to(Vector2(reletivePoint.x,reletivePoint.z))
		$Compass.rotation = angle
		pass
	else:
		visible = false

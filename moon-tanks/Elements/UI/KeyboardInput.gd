extends Node

export(float) var mouseSensitivity = 1.0



var mouseDelta := Vector2.ZERO


func get_controls()->ControlsContainer:
	var output:ControlsContainer=ControlsContainer.new();
#	var movement:Vector3 = Vector3.ZERO;
#	var jumped:bool = false;
	if Input.is_action_pressed("Forward"):
		output.movement += Vector3.FORWARD
	if Input.is_action_pressed("Back"):
		output.movement += Vector3.BACK
	if Input.is_action_pressed("Left"):
		output.movement += Vector3.LEFT
	if Input.is_action_pressed("Right"):
		output.movement += Vector3.RIGHT

	output.jumped = Input.is_action_pressed("Jump")
	output.crouch = Input.is_action_pressed("Crouch")
	output.renormalize()
	output.look = mouseDelta
	mouseDelta = Vector2.ZERO
	
	output.isFiring = Input.is_action_pressed("Shoot")

	
	return output
	
	


func _unhandled_input(event):
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if event is InputEventMouseMotion:
			mouseDelta += event.relative * mouseSensitivity * -0.01
	
	
"""
this control system should be portable to contorller, just needs to return a ControlsContainer


On pause, release mouse.
On resume, capture mouse, hand off to this guy. 
if mouse captured:
	mouse xy movement collected
	
get_controls
	mousexy movement is recorded and cleared.
	if mouse1/2, return shooting commands




"""
	

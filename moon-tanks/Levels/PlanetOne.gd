extends VoxelLodTerrain

class_name PlanetOne

export(float) var spawnHight

var myRand:=RandomNumberGenerator.new()

func _ready():
	myRand.randomize()
	stream.noise.seed = myRand.randi()
	Pivot.Planet = self
	$FallThroughVolume.scale = Vector3.ONE * stream.trueMin - (Vector3.ONE * 25)
	

	
func getPoint(p:Vector3)->Vector3:
	if p == Vector3.ZERO:
		p = Vector3.FORWARD
	var pn = p.normalized()
	var pointValue = stream.getPoint(pn)
	var output = pn * (-pointValue)
	return output


func getSurfaceTransform(source:Transform)->Transform: #this is going to cause some sort of crash, near some axis, but I'm not sure what it will be yet
	var origin = getPoint(source.origin)
	var scale = source.basis.get_scale()
	
	var up :Vector3= source.origin.normalized()
	var back :Vector3= source.xform(Vector3.BACK)
	back = (back - back.project(up))
	var right = up.cross(back)
	var basis = Basis(right,up,back).scaled(scale)

	return Transform(basis,origin)

func _on_Area_body_entered(body:PhysicsBody):
	if body:
		if body.has_method("isPlayer") and body.isPlayer():
			var p = getPoint(body.global_transform.origin)
			body.global_transform = Transform(Basis(),p + (p.normalized()*spawnHight))
		else:
			body.queue_free()

func makeActiveWatcher(watcher:Node):
	viewer_path = get_path_to(watcher)


func dig(origin,radius):
	var voxelTool :VoxelTool= get_voxel_tool()
	voxelTool.mode = VoxelTool.MODE_REMOVE
	voxelTool.value = radius
	voxelTool.do_sphere(origin, radius)


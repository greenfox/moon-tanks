tool
extends VoxelGenerator

class_name VoxelSphere

#export var amplitude:float = 10.0
#export var period:Vector2 = Vector2(1/10.0, 1/10.0)
#export var iso_scale:float = 0.1

export(float) var radius = 2048 setget setRadius
export(float) var noiseAmplitude = 128 setget setNoiseAmp
export(float) var floorThreshhold = 0.0 setget setFloorTresh

export(OpenSimplexNoise) var noise
export(float) var noiseScale = 100;

var trueMin
var trueMax
var seaHeight
#float sdBox( vec3 p, vec3 b )
func sdBox(p:Vector3, b:Vector3)->float:
#  vec3 q = abs(p) - b;
	var q := p.abs() - b
#  return  length(max(q,0.0)) 								  + min(max(q.x,max(q.y,q.z)),0.0);
	return Vector3(max(q.x,0),max(q.y,0),max(q.z,0)).length() +  min(max(max(q.x,q.y),q.z),0);

func sdSphere(p:Vector3, r:float)->float:
#float sdSphere( vec3 p, float s )
#{
	return p.length()-r
#  return length(p)-s;
#}

func generate_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int) -> void:
	
	var stride:int = 1 << lod
	var size:Vector3 = out_buffer.get_size()
#	var centerToEdge = size.x*0.
	var halfEdgeSize = stride*8
	var center := origin + Vector3(halfEdgeSize,halfEdgeSize,halfEdgeSize)
		
	if center.length() > (trueMax+halfEdgeSize):
#		print("dropped outside chunk",{center=center,size=halfEdgeSize})
		out_buffer.fill(-1,VoxelBuffer.CHANNEL_SDF)
		return
	if center.length() < max(0,trueMin - (halfEdgeSize*1.732)):
#		print("dropped inside chunk",{center=center,size=halfEdgeSize})
		out_buffer.fill(1,VoxelBuffer.CHANNEL_SDF)
		return


	var g:Vector3 = Vector3.ZERO
	g.z = origin.z
	for z in range(0, size.z):
		g.x = origin.x
		for x in range(0, size.x):
			g.y = origin.y
			for y in range(0, size.y):
				out_buffer.set_voxel_f(getPoint(g)/stride,x, y, z, VoxelBuffer.CHANNEL_SDF)
				g.y += stride
			g.x += stride
		g.z += stride



func getPoint(g:Vector3)->float:
	var value = g.length()-radius
	var noiseValue = noise.get_noise_3dv(g.normalized()*noiseScale) * noiseAmplitude
	value -= max(noiseValue*floorThreshhold,noiseValue)
	return value


func setConsts():
	trueMin = radius - (noiseAmplitude*floorThreshhold)
	trueMax = radius + noiseAmplitude
	seaHeight = radius + (noiseAmplitude*floorThreshhold)
	

func setRadius(value):
	radius = value
	setConsts()

func setNoiseAmp(value):
	noiseAmplitude = value
	setConsts()

func setFloorTresh(value):
	floorThreshhold = value
	setConsts()

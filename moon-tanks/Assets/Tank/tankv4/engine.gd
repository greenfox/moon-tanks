extends Spatial

export(NodePath) var tankPath = NodePath ( ".." )
export(NodePath) var enginePath

var thisPosition
var lastTransform
var thisTransform

func _ready ( ):
	thisTransform = get_global_transform ( )
	lastTransform = thisTransform
	var tank = get_node ( tankPath )
	var engine = get_node ( enginePath )
	assert(get_node ( tankPath ), "tankPath missing in engine")
	assert(get_node ( enginePath ), "enginePath missing in engine")

func _process ( delta ):
	var tank = get_node ( tankPath )
	var engine = get_node ( enginePath )
	thisTransform = get_global_transform ( )
	var forward = lastTransform.basis.z
	var right = lastTransform.basis.x
	var ds = thisTransform.origin - lastTransform.origin
	var dz = ds.dot ( forward )
	var dx = ds.dot ( right )
	engine.rotation.x = tank.xRotationMultiplier * dz
	#TODO: exaggerate z rotation when tank is rotating
	var s = sign ( translation.z )
	engine.rotation.z = -tank.zRotationMultiplier * ( dx + s * tank.rotationalVelocity )
	lastTransform = thisTransform

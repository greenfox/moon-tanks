extends Spatial

export(NodePath) var chassisPath
export var xRotationMultiplier = 0.5
export var zRotationMultiplier = 1.3
export var xBodyRotation = 1.0
export var zBodyRotation = 1.0

var thisTransform: Transform
var lastTransform: Transform
var lastDx := 0.0
var lastDz := 0.0
var smoothXAcceleration := 0.0
var smoothZAcceleration := 0.0
var rotationalVelocity := 0.0 #engines read this to exaggerate torque for turning

func _ready ( ):
	thisTransform = get_global_transform ( )
	lastTransform = thisTransform
	assert ( get_node ( chassisPath ), "chassis missing in tankSystems" )

func _process ( delta ):
	var chassis = get_node ( chassisPath )
	thisTransform = get_global_transform ( )
	var forward = lastTransform.basis.z
	var right = lastTransform.basis.x
	var ds = thisTransform.origin - lastTransform.origin
	var thisDx = ds.dot ( right )
	var thisDz = ds.dot ( forward )
	var xAcceleration = thisDx - lastDx
	var zAcceleration = thisDz - lastDz
	
	smoothXAcceleration = lerp ( smoothXAcceleration , xAcceleration , 5.0 * delta )
	smoothZAcceleration = lerp ( smoothZAcceleration , zAcceleration , 5.0 * delta )
	
	rotation.x = xBodyRotation * smoothZAcceleration
	rotation.z = -zBodyRotation * smoothXAcceleration
	lastTransform = thisTransform

	#last-minute fix: set rotationalMultiplier to increase engine rotation
	# when turning
	var turning = forward.cross ( thisTransform.basis.z )
	
	var thisRotation = 50.0 * turning.dot ( thisTransform.basis.y )
	rotationalVelocity = lerp ( rotationalVelocity , thisRotation , 5.0 * delta )

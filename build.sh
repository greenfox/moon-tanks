#!/bin/bash


main()
{
	pushd moon-tanks
	rm bin -f
	ln -s ../bin .
	builder
	mv public ..
	rm bin
}

cd $(dirname $0)
main
